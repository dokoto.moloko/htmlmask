var StringXml = null;
var HasAjaxResponse = false;
var gid = null;
var chkeventid = null;

function GetSelectValueFromListBox(ListBoxId)
{
	listBox = document.getElementById(ListBoxId);
	return listBox.options[listBox.selectedIndex].value;
}
function hiddeObjs()
{
	var theForm = document.getElementById('form1');
	for(var i = 0; i < theForm.bt.length; i++)
        {
			theForm.bt[i].style.visibility='hidden';
        }
}
function javaGetFullPath(id)
{
        var app = document.FilePath;
        document.getElementById(id).value = app.GetFullPathToFileSec();
}
function ExecuteCMD()
{
	var xmlhttp;
	if (window.XMLHttpRequest)
	{
		/*code for IE7+, Firefox, Chrome, Opera, Safari*/
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		/* code for IE6, IE5*/
		xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
	}
	document.getElementById('results').innerHTML = "<br /><div id = 'result'>Ejecutando...</div>";
	xmlhttp.open('POST','execute.do',true);
	var params = BuildParams();
	xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	xmlhttp.setRequestHeader('Content-length', params.length);
	xmlhttp.setRequestHeader('Connection', 'close');
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			document.getElementById('results').innerHTML = "<br /><div id = 'result'> Proceso Finalizado.. " + xmlhttp.responseText + "</div>";
		}
	}
	xmlhttp.send(params);
}

function INITfileBrowser(id, tipo, nombre, ruta)
{
	gid = id;
	chkeventid = self.setInterval("checkAjaxResponse()", 100);
	var imp = document.getElementById(id).value;
	if (imp.length > 0)
		ruta = 	document.getElementById(id).value;
	GetListFiles(tipo, nombre, ruta);
}


function fileBrowser(id, tipo, nombre, ruta)
{
	gid = id;
	chkeventid = self.setInterval("checkAjaxResponse()", 100);
	GetListFiles(tipo, nombre, ruta);
}

function GetFileInfo(id, tipo, nombre, ruta)
{
	if (tipo == 'F')
	{
		document.getElementById(id).value = ruta;
		document.getElementById('layer_scroll').style.visibility='hidden';
	}
	else if (tipo == 'R' || tipo == 'D')
	{
		fileBrowser(id, tipo, nombre, ruta)
	}
}
function GetListFiles(tipo, nombre, ruta)
{
	var xmlhttp = null;
	StringXml = null;
	HasAjaxResponse = false;
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.open('POST','execute.do',true);
	var params = 'cmd=list_files&ruta=' + encodeURIComponent(ruta);
	params += '&tipo=' + encodeURIComponent(tipo);
	xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	xmlhttp.setRequestHeader('Content-length', params.length);
	xmlhttp.setRequestHeader('Connection', 'close');
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
		{
			StringXml = xmlhttp.responseText;
			HasAjaxResponse = true;
		}
	}
	xmlhttp.send(params);
}
function StringtoXML(text)
{
	if (text == null)
		return null;
	if (text.length == 0)
		return null;
	if (window.ActiveXObject)
	{
		var doc=new ActiveXObject('Microsoft.XMLDOM');
        doc.async='false';
        doc.loadXML(text);
    }
	else
	{
		var parser=new DOMParser();
        var doc=parser.parseFromString(text,'text/xml');
	}
	return doc;
}
function fixWinPath(path)
{
	if (path.substring(1,3) == ':\\')
	{
		return path.replace(/\\/g,'\\\\');
	}
    else
    {
        return path;
    }
}
function checkAjaxResponse()
{
	if (HasAjaxResponse == true)
	{
		printFiles(gid);
		HasAjaxResponse = false;
		clearInterval(chkeventid);
	}
}
function printFiles(id)
{
	var xmlDoc = null;
	var div_links = document.getElementById('layer_scroll');
	div_links.innerHTML = "";
	xmlDoc = StringtoXML( StringXml );
	if (xmlDoc != null)
	{
		 var links_tag = xmlDoc.getElementsByTagName("lista")[0].getElementsByTagName("item");
		 for (var i = 0; i < links_tag.length; i++)
		 {
			  var tipo = links_tag[i].getElementsByTagName("tipo")[0].childNodes[0].nodeValue;
			  var nombre = links_tag[i].getElementsByTagName("nombre")[0].childNodes[0].nodeValue;
			  var ruta = links_tag[i].getElementsByTagName("ruta")[0].childNodes[0].nodeValue;
			  ruta = fixWinPath(ruta);
			  div_links.innerHTML += "<p><a href='javascript:GetFileInfo(\"" + id + "\",\"" + tipo + "\",\"" + nombre + "\",\"" + ruta + "\")'>" + nombre + "</a></p>";
		 }
		document.getElementById('layer_scroll').style.visibility='visible';
	} else alert('[ERROR] No se han recuperado datos del servidor.');
}
function BuildParams() {
var params = 'cmd=execute&' + 'html_y=' + document.getElementById('html_y').value;
params += '&' + 'html_i=' + document.getElementById('html_i').value;
params += '&' + 'html_ss=' + document.getElementById('html_ss').value;
params += '&' + 'html_t=' + document.getElementById('html_t').value;
params += '&' + 'html_threads=' + document.getElementById('html_threads').value;
params += '&' + 'html_s=' + GetSelectValueFromListBox('html_s');
params += '&' + 'html_r=' + document.getElementById('html_r').value;
params += '&' + 'html_pix_fmt=' + document.getElementById('html_pix_fmt').value;
params += '&' + 'html_gt=' + document.getElementById('html_gt').value;
params += '&' + 'html_qmin=' + document.getElementById('html_qmin').value;
params += '&' + 'html_b=' + document.getElementById('html_b').value;
params += '&' + 'html_async=' + document.getElementById('html_async').value;
params += '&' + 'html_acodec=' + document.getElementById('html_acodec').value;
params += '&' + 'html_ar=' + document.getElementById('html_ar').value;
params += '&' + 'html_ac=' + document.getElementById('html_ac').value;
params += '&' + 'html_ab=' + document.getElementById('html_ab').value;
params += '&' + 'html_out=' + document.getElementById('html_out').value;
return params; }
