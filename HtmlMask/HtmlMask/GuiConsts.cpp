#include "GuiConsts.hpp"

namespace htmk 
{

	/*--------------------------------------------------------------------------------------------------------------
	* CONSTANTS HTML
	*--------------------------------------------------------------------------------------------------------------*/  
	CCStr_t html_begin = "\
<html>\n\
	<head>\n\
		<meta meta name=\"App-server\"  content=\"text/html;\" http-equiv=\"content-type\" charset=\"utf-8\" />\n\
		<link href='css/index.css' rel='stylesheet' type='text/css' />\n\
		<script src='javascript/ajax.js' type='application/javascript'></script>\n\
	</head>\n\
	<body>\n\
		<div id = \"layer_scroll\"></div>\n\
		<div id = 'contenido'>\n\
			<div id = 'encuadre'>\n\
				<div id = 'texto'>\n\
					<form id='form1'>\n";

	 CCStr_t html_end = "\
					</form>\n\
			</div>\n\
				<div id = 'line'>\n\
					<button id='btnSearch' type='button' onclick='ExecuteCMD()'>Ejecutar</button>\n\
				</div>\n\
			</div>\n\
			<div id='results'></div>\n\
		</div>\n\
	</body>\n\
	<!-- \n\
	--------------------------------------------------------------------------------------------------------------------------------------------------\n\
	APPLE JAVA : Debido a las POLLECES de seguridad habituales, HTML-javaScript no permite acceso al valor del INPUT FILE, ni enfocarlo, ni seleccionar\n\
	su texto. Por lo que para crear algo parecido se llama a un applet java que muestra un dialogo que asiste en la busqueda del fichero\n\
	emulando asi el comportamiento del INPUT FILE.\n\
	--------------------------------------------------------------------------------------------------------------------------------------------------\n\
    <noscript>A browser with JavaScript enabled is required for this page to operate properly.</noscript>\n\
	<script type='text/javascript' src='http://www.java.com/js/deployJava.js'></script>\n\
	<script type='text/javascript'>\n\
		alert('Debido a las POLLECES de seguridad habituales, HTML-javaScript no permite acceso al valor del INPUT FILE, ni enfocarlo, ni seleccionar \
su texto. Por lo que para crear algo parecido se llama a un applet java que muestra un dialogo que asiste en la busqueda del fichero \
emulando asi el comportamiento del INPUT FILE. Se le va ha solicitar que confie en el Applet si desea esta utilidad diga si.');\n\
		if (navigator.javaEnabled())\n\
		{\n\
			var attributes = { id:'FilePath', code:'appletUtils.FileHelper.class', archive:'appletUtils/FileHelper_Signed.jar', width:50, height:50 } ;\n\
			var parameters = {fontSize:16} ;\n\
			deployJava.runApplet(attributes, parameters, '1.6');\n\
		}\n\
		else\n\
		{\n\
			alert('Debido a que no se detecta java instalado, se desactiva el applet de ayuda para seleccionar ficheros.');\n\
			hiddeObjs();\n\
		}\n\
	</script>-->\n\n\
</html>\n";

	CCStr_t div_end = "</div>";
	CCStr_t div_line_begin = "<div id = 'line' %s>";
	CCStr_t div_label = "<div id = 'text_label'>CMD ARG %s [%s] : -%s</div>";
	CCStr_t input = "<input type='%s' size='%s' maxlength='%s' id='%s' value='%s' %s />";
	CCStr_t disable = "disabled='disabled'";
	CCStr_t hidden = "hidden";
	CCStr_t select_default = "selected='selected'";
	CCStr_t button_help_path = "<button id='%s' type='button' onclick=\"INITfileBrowser('%s', '%s', '%s', '%s')\" %s>Explorar</button>";	
	CCStr_t listBox_begin = "<select id='%s' readonly>";
	CCStr_t listBox_opcion = "<option value='%s' %s %s>%s</option>";
	CCStr_t listBox_end = "</select>";

	/*--------------------------------------------------------------------------------------------------------------
	* CONSTANTS JAVASCRIPT
	*--------------------------------------------------------------------------------------------------------------*/  
	CCStr_t js_globals = "\
var StringXml = null;\n\
var HasAjaxResponse = false;\n\
var gid = null;\n\
var chkeventid = null;\n\
\n";

	CCStr_t js_rel_path = "javascript/ajax.js";
	CCStr_t js_ExecuteCMD  = "\
function ExecuteCMD()\n\
{\n\
	var xmlhttp;\n\
	if (window.XMLHttpRequest)\n\
	{\n\
		/*code for IE7+, Firefox, Chrome, Opera, Safari*/\n\
		xmlhttp=new XMLHttpRequest();\n\
	}\n\
	else\n\
	{\n\
		/* code for IE6, IE5*/\n\
		xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');\n\
	}\n\
	document.getElementById('results').innerHTML = \"<br /><div id = 'result'>Ejecutando...</div>\";\n\
	xmlhttp.open('POST','execute.do',true);\n\
	var params = BuildParams();\n\
	xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');\n\
	xmlhttp.setRequestHeader('Content-length', params.length);\n\
	xmlhttp.setRequestHeader('Connection', 'close');\n\
	xmlhttp.onreadystatechange=function()\n\
	{\n\
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)\n\
		{\n\
			document.getElementById('results').innerHTML = \"<br /><div id = 'result'> Proceso Finalizado.. \" + xmlhttp.responseText + \"</div>\";\n\
		}\n\
	}\n\
	xmlhttp.send(params);\n\
}\n";

	CCStr_t js_GetSelectValueFromListBox = "\
function GetSelectValueFromListBox(ListBoxId)\n\
{\n\
	listBox = document.getElementById(ListBoxId);\n\
	return listBox.options[listBox.selectedIndex].value;\n\
}\n";

	CCStr_t js_INITfilebrowser = "\
function INITfileBrowser(id, tipo, nombre, ruta)\n\
{\n\
	gid = id;\n\
	chkeventid = self.setInterval(\"checkAjaxResponse()\", 100);\n\
	var imp = document.getElementById(id).value;\n\
	if (imp.length > 0)\n\
		ruta = 	document.getElementById(id).value;\n\
	GetListFiles(tipo, nombre, ruta);\n\
}\n";
              
    CCStr_t js_filebrowser = "\
function fileBrowser(id, tipo, nombre, ruta)\n\
{\n\
	gid = id;\n\
	chkeventid = self.setInterval(\"checkAjaxResponse()\", 100);\n\
	GetListFiles(tipo, nombre, ruta);\n\
}\n";

	CCStr_t js_checkAjaxResponse = "\
function checkAjaxResponse()\n\
{\n\
	if (HasAjaxResponse == true)\n\
	{\n\
		printFiles(gid);\n\
		HasAjaxResponse = false;\n\
		clearInterval(chkeventid);\n\
	}\n\
}\n";

	CCStr_t js_printFiles = "\
function printFiles(id)\n\
{\n\
	var xmlDoc = null;\n\
	var div_links = document.getElementById('layer_scroll');\n\
	div_links.innerHTML = \"\";\n\
	xmlDoc = StringtoXML( StringXml );\n\
	if (xmlDoc != null)\n\
	{\n\
		 var links_tag = xmlDoc.getElementsByTagName(\"lista\")[0].getElementsByTagName(\"item\");\n\
		 for (var i = 0; i < links_tag.length; i++)\n\
		 {\n\
			  var tipo = links_tag[i].getElementsByTagName(\"tipo\")[0].childNodes[0].nodeValue;\n\
			  var nombre = links_tag[i].getElementsByTagName(\"nombre\")[0].childNodes[0].nodeValue;\n\
			  var ruta = links_tag[i].getElementsByTagName(\"ruta\")[0].childNodes[0].nodeValue;\n\
			  ruta = fixWinPath(ruta);\n\
			  div_links.innerHTML += \"<p><a href='javascript:GetFileInfo(\\\"\" + id + \"\\\",\\\"\" + tipo + \"\\\",\\\"\" + nombre + \"\\\",\\\"\" + ruta + \"\\\")'>\" + nombre + \"</a></p>\";\n\
		 }\n\
		document.getElementById('layer_scroll').style.visibility='visible';\n\
	} else alert('[ERROR] No se han recuperado datos del servidor.');\n\
}\n";

	CCStr_t js_getfileinfo = "\
function GetFileInfo(id, tipo, nombre, ruta)\n\
{\n\
	if (tipo == 'F')\n\
	{\n\
		document.getElementById(id).value = ruta;\n\
		document.getElementById('layer_scroll').style.visibility='hidden';\n\
	}\n\
	else if (tipo == 'R' || tipo == 'D')\n\
	{\n\
		fileBrowser(id, tipo, nombre, ruta);\n\
	}\n\
}\n";

	CCStr_t js_getlistfiles = "\
function GetListFiles(tipo, nombre, ruta)\n\
{\n\
	var xmlhttp = null;\n\
	StringXml = null;\n\
	HasAjaxResponse = false;\n\
	if (window.XMLHttpRequest)\n\
	{\n\
		xmlhttp=new XMLHttpRequest();\n\
	}\n\
	else\n\
	{\n\
		xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');\n\
	}\n\
	xmlhttp.open('POST','execute.do',true);\n\
	var params = 'cmd=list_files&ruta=' + encodeURIComponent(ruta);\n\
	params += '&tipo=' + encodeURIComponent(tipo);\n\
	xmlhttp.setRequestHeader('Content-type','application/x-www-form-urlencoded');\n\
	xmlhttp.setRequestHeader('Content-length', params.length);\n\
	xmlhttp.setRequestHeader('Connection', 'close');\n\
	xmlhttp.onreadystatechange=function()\n\
	{\n\
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200)\n\
		{\n\
			StringXml = xmlhttp.responseText;\n\
			HasAjaxResponse = true;\n\
		}\n\
	}\n\
	xmlhttp.send(params);\n\
}\n";

	CCStr_t js_hiddeObjs = "\
function hiddeObjs()\n\
{\n\
	var theForm = document.getElementById('form1');\n\
	for(var i = 0; i < theForm.bt.length; i++)\n\
        {\n\
			theForm.bt[i].style.visibility='hidden';\n\
        }\n\
}\n";

	CCStr_t js_javaGetFullPath ="\
function javaGetFullPath(id)\n\
{\n\
        var app = document.FilePath;\n\
        document.getElementById(id).value = app.GetFullPathToFileSec();\n\
}\n";

	CCStr_t js_StringtoXml ="\
function StringtoXML(text)\n\
{\n\
	if (text == null)\n\
		return null;\n\
	if (text.length == 0)\n\
		return null;\n\
	if (window.ActiveXObject)\n\
	{\n\
		var doc=new ActiveXObject('Microsoft.XMLDOM');\n\
        doc.async='false';\n\
        doc.loadXML(text);\n\
    }\n\
	else\n\
	{\n\
		var parser=new DOMParser();\n\
        var doc=parser.parseFromString(text,'text/xml');\n\
	}\n\
	return doc;\n\
}\n";

	CCStr_t js_fixWinPath ="\
function fixWinPath(path)\n\
{\n\
	if (path.substring(1,3) == ':\\\\')\n\
	{\n\
		return path.replace(/\\\\/g,'\\\\\\\\');\n\
	}\n\
    else\n\
    {\n\
        return path;\n\
    }\n\
}\n";

	CCStr_t js_BuildParams_begin = "function BuildParams() {";
	CCStr_t js_BuildParams_add_first_input = "var params = 'cmd=execute&' + '%s=' + document.getElementById('%s').value;";
	CCStr_t js_BuildParams_add_input = "params += '&' + '%s=' + document.getElementById('%s').value;";
	CCStr_t js_BuildParams_add_first_listBox = "var params = '%s=' + GetSelectValueFromListBox('%s');";
	CCStr_t js_BuildParams_add_listBox = "params += '&' + '%s=' + GetSelectValueFromListBox('%s');";
	CCStr_t js_BuildParams_end = "return params; }";


	/*--------------------------------------------------------------------------------------------------------------
	* CONSTANTS JAVA
	*--------------------------------------------------------------------------------------------------------------*/
	CCStr_t java_rel_path = "java/FileHelper.jar";

	/*--------------------------------------------------------------------------------------------------------------
	* CONSTANTS CSS
	*--------------------------------------------------------------------------------------------------------------*/  	
	CCStr_t css_rel_path = "css/index.css";
	CCStr_t css = "\
@charset 'UTF-8';\n\
.body {\n\
	font-family: Geneva, Arial, Helvetica, sans-serif;\n\
	text-align:center;\n\
}\n\
A:link, A:visited { text-decoration: none }\n\
#layer_scroll{\n\
	font-family: Geneva, Arial, Helvetica, sans-serif;\n\
	position:absolute;\n\
	z-index: 100;\n\
	overflow: auto;\n\
	height: 200px;\n\
	width: 600px;\n\
	border: 10px solid rgb(25,220,215);\n\
	background-color: rgb(250,250,255);\n\
	padding: 8px;\n\
	top: 50px;\n\
	margin-left: auto;\n\
	margin-right: auto;\n\
	left:0;\n\
	right:0;\n\
	visibility:hidden;\n\
}\n\
#contenido{\n\
	position:relative;\n\
	float:left;\n\
	margin-top:5%;\n\
	width:100%;\n\
	text-align:center;\n\
	border:0px black solid;\n\
	}\n\
#encuadre{\n\
	background-color:rgb(250,250,255);\n\
	padding-top:2px;\n\
	padding-bottom:2px;\n\
	margin-left: auto ;\n\
	margin-top: 2px ;\n\
	height: auto;\n\
	width: auto;\n\
	margin-right: auto ;\n\
	border:1px black solid;\n\
}\n\
#text_label{\n\
	font-family: Geneva, Arial, Helvetica, sans-serif;\n\
	font-size: 12px;\n\
	font-weight: 900;\n\
	color: rgb(10, 10, 10);\n\
}\n\
#line{\n\
	width:600px;\n\
	margin:0px auto;\n\
	text-align:left;\n\
	padding:15px;\n\
	border:0px black solid;\n\
}\n\
#result{\n\
	margin-top:5px;\n\
	font-family: Geneva, Arial, Helvetica, sans-serif;\n\
	font-size: 12px;\n\
	background-color:rgb(238,232,170);\n\
	width:600px;\n\
	margin:0px auto;\n\
	text-align:left;\n\
	padding:15px;\n\
	border:1px blue solid;\n\
}\n\
#helper{\n\
	width:600px;\n\
	margin:0px auto;\n\
	text-align:left;\n\
	padding:15px;\n\
	background-color:rgb(255,99,71);\n\
	border:2px blue solid;\n\
}\n";

}

