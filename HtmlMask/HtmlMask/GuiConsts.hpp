#ifndef GUICONSTS_HPP
#define GUICONSTS_HPP

namespace htmk 
{

/*--------------------------------------------------------------------------------------------------------------
* CONSTANTS TYPES
*--------------------------------------------------------------------------------------------------------------*/  
typedef const char*		                                CCStr_t;

extern CCStr_t html_top;
extern CCStr_t html_begin;
extern CCStr_t html_end;
extern CCStr_t div_end;
extern CCStr_t div_line_begin;
extern CCStr_t div_label;
extern CCStr_t input;
extern CCStr_t button_help_path;
extern CCStr_t listBox_begin;
extern CCStr_t listBox_opcion;
extern CCStr_t listBox_end;
extern CCStr_t js_ExecuteCMD;
extern CCStr_t js_GetSelectValueFromListBox;
extern CCStr_t js_hiddeObjs;
extern CCStr_t js_filebrowser;
extern CCStr_t js_INITfilebrowser;
extern CCStr_t js_getfileinfo;
extern CCStr_t js_getlistfiles;
extern CCStr_t js_BuildParams_begin;
extern CCStr_t js_BuildParams_add_first_input;
extern CCStr_t js_BuildParams_add_input;
extern CCStr_t js_BuildParams_add_first_listBox;
extern CCStr_t js_BuildParams_add_listBox;
extern CCStr_t js_BuildParams_end;
extern CCStr_t js_StringtoXml;
extern CCStr_t js_fixWinPath;
extern CCStr_t js_checkAjaxResponse;
extern CCStr_t js_printFiles;
extern CCStr_t js_globals;
extern CCStr_t css;
extern CCStr_t js_rel_path;
extern CCStr_t css_rel_path;
extern CCStr_t java_rel_path;
extern CCStr_t js_javaGetFullPath;
extern CCStr_t disable;
extern CCStr_t hidden;
extern CCStr_t select_default;

}

#endif //GUICONSTS_HPP


