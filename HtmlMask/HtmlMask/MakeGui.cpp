#include "MakeGui.hpp"

namespace htmk 
{
	MakeGui::MakeGui(CoCStr_t conf_path, CoCStr_t confmap_path) :
	mc_fn_errors("errores.log"),
	mc_fn_results("resultados.log"),
	mc_fn_cmd("cmd.bat"),
	mc_unix_sep("/"),
	mc_win_sep("\\"),
	mc_line_sep('|'),
	mc_value_sep('#'),
	mc_conf_path(conf_path),
	mc_map_path(confmap_path)
	{
		setDirSep(conf_path);
	}

	MakeGui::~MakeGui(void)
	{
		if (m_fileOut.is_open())
			m_fileOut.close();
		if (m_fileInput.is_open())
			m_fileInput.close();
	}

	bool MakeGui::Generate()
	{
	  	if (!m_file_config.Load(mc_conf_path) ) return false;
		if (!m_file_maps.Load(mc_map_path) ) return false;
		if (!buildHtml()) return false;
		if (!buildJavaScript()) return false;
		if (!buildCSS()) return false;
		//if (!buildJava()) return false;
		#ifdef WIN
			std::cout << std::endl << "Pulse cualquier tecla para terminar..." << std::endl;
			std::cin.get();
		#endif
		return true;
	}

	MakeGui::Str_t MakeGui::FixWinPath(CoStr_t& path)
	{
		size_t len = path.size();
		if (len == 0) return "";
		Str_t n_path;
		n_path.resize(len+1);
		char* c_str = new char[len+1];		
		std::memset(c_str, 0, len+1);
		std::strncpy(c_str, path.c_str(), len);
		size_t x = 0;
		for(size_t i = 0; i < len; i++)
		{
			if (*(c_str+i) == *mc_win_sep && *(c_str+i+1) != *mc_win_sep)
			{
				n_path.resize(n_path.size()+1);
				n_path[x++] = (*mc_win_sep);
			}
			n_path[x++] = *(c_str+i);
		}
		delete[] c_str;
		c_str = NULL;

		return n_path;
	}

	bool MakeGui::readTextFile(CoStr_t& path, Str_t& file)
	{			
		//m_fileInput.open(FixWinPath(path).c_str());
		m_fileInput.open(htmk::utils::FixWinPath(path).c_str());
		if (m_fileInput.is_open())
		{            
			Str_t line;
			m_fileInput.seekg(0, std::ios::beg);
			Buffer_t ss;
			ss << m_fileInput.rdbuf();
			file = ss.str();
			m_fileInput.close();
			if (file.size() == 0) return false;

		} else return false;

		return true;
	}

	bool MakeGui::ExecuteSystemCmd(CoStr_t& cmd)
	{	
		std::printf("Begining to Execute Command...");	
		Str_t lcmd(cmd);
	#ifdef WIN					
		m_fileOut.open(mc_fn_cmd);
		m_fileOut << lcmd;
		m_fileOut.close();		
		lcmd = mc_fn_cmd;
	#endif
		system(lcmd.c_str());

		Str_t m_result_file_log, m_result_file_errors;
		readTextFile(mc_fn_results, m_result_file_log);
		readTextFile(mc_fn_errors, m_result_file_errors);
		if (m_result_file_errors.empty())
		{
			std::printf(" Finish OK.\n");
			return true;
		}
		else
		{				
			std::printf(" Finish with Errors, check log files.\n");
			#if defined LINUX || defined APPLE		
			  std::printf("\n\n========================== TRACE ====================================\n");
			  std::printf("%s\n", lcmd.c_str());
			  std::printf("\n========================== TRACE ====================================\n");
			#endif			
			return false;
		}
	}

	bool MakeGui::setDirSep(CoStr_t& path)
	{
		Str_t like_unix = path.substr(0, 1);
		Str_t like_win  = path.substr(1, 2);

		if ( like_unix.compare("/") == 0 )
			m_dir_sep = mc_unix_sep;
		else if ( like_win.compare(":\\") == 0 ) 
			m_dir_sep = mc_win_sep;

		return true;
	}


	bool FuncSort1(const MakeGui::CmdArgsSorted_t& left, const MakeGui::CmdArgsSorted_t& right)
	{
		return left.position < right.position;
	}

	void MakeGui::sortArgs(const MapSS_t& data, VCmdArgsSorted_t& vArgs)
	{
		for(MapSS_t::const_iterator it = data.begin(); it != data.end(); it++)
		{
			Str_t PosValue;
			Buffer_t conv;
			size_t pos = 0;
			m_file_maps.GetAttValFromFirst(it->first, m_file_maps.mc_att_position, PosValue);
			conv << PosValue;
			conv >> pos;
			CmdArgsSorted_t wa = {pos, it->first};
			vArgs.push_back(wa);
		}
		std::sort(vArgs.begin(), vArgs.end(), FuncSort1);
	}

	void MakeGui::BuildInputs(CoStr_t& CmdParam, CoStr_t& HtmlParam, FileOut_t& fileOut)
	{
		Str_t id_cmd, id_html, type_html, size_html, maxsize_html, value_html, hidden_html, position_html, label_html;
		Str_t disable_html, disable_value, hidden_value;
		size_t len = 0;
		char* line = NULL;

		m_file_maps.GetAttValFromFirst(CmdParam, m_file_maps.mc_att_id, id_cmd);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_id, id_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_type, type_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_size, size_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_maxsize, maxsize_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_value, value_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_hidden, hidden_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_label, label_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_disable, disable_html);		
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_hidden, hidden_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_value, value_html);

		m_file_maps.GetAttValFromFirst(CmdParam, m_file_maps.mc_att_position, position_html);
				
		// <div id = "line" %s>;
		len = std::strlen(htmk::div_line_begin);
		if (hidden_html.compare(m_file_maps.mc_val_true) == 0)
		{			
			hidden_value = htmk::hidden;
			len += hidden_value.size();
		}
		len += 10;
		line = new char[len];
		std::memset(line, 0, len);
		std::sprintf(line, htmk::div_line_begin, hidden_value.c_str());
		fileOut << line << std::endl;
		delete[] line;
		line = NULL;

		// <div id = "text_label">CMD ARG 1 [Descripcion]: -fich1_datos</div>
		// div_label = "<div id = 'text_label'>CMD ARG %s [%s]: -%s</div>"
		len = std::strlen(htmk::div_label) + position_html.size() + label_html.size() + id_cmd.size() + 10;
		line = new char[len];
		std::memset(line, 0, len);
		std::sprintf(line, htmk::div_label, position_html.c_str(), label_html.c_str(), id_cmd.c_str());
		fileOut << line << std::endl;
		delete[] line;
		line = NULL;

		// <input type="text" size="60" maxlength="255" id="html_fich1_datos" />
		// input =  "<input type='%s' size='%s' maxlength='%s' id='%s' value='%s' %s/>"
		// El ultimo '%s' es para disable="disable"
		len = std::strlen(htmk::input) + std::strlen("text") + size_html.size() + maxsize_html.size() + id_html.size() + value_html.size(); 
		if (disable_html.compare(m_file_maps.mc_val_true) == 0)
		{			
			disable_value = htmk::disable;
			len += disable_value.size();
		}
	    len += 10;

		line = new char[len];
		std::memset(line, 0, len);
		std::sprintf(line, htmk::input, "text", size_html.c_str(), maxsize_html.c_str(), id_html.c_str(), value_html.c_str(), disable_value.c_str());
		fileOut << line << std::endl;
		delete[] line;
		line = NULL;

		// <button id='bt1' type="button" onclick="javaGetFullPath('html_fich1_datos')">Explorar</button>		
		// "<button id='%s' type='button' onclick=\"fileBrowser('%s', '%s', '%s', '%s')\" %s>Explorar</button>";
		// fileBrowser('html_len', 'D', null, 'C:\\')
		if (type_html.compare("file") == 0)
		{
			hidden_html.clear();
			disable_html.clear();
			len = std::strlen(htmk::button_help_path) + std::strlen("bt") + id_html.size() +
				std::strlen("D") + std::strlen("null") + htmk::utils::CurrentDir().size();
			if (disable_html.compare(m_file_maps.mc_val_true) == 0)
			{
				disable_value = htmk::disable;
				len += disable_value.size();				
			}					
			len += 10;
			line = new char[len];
			std::memset(line, 0, len);
			std::sprintf(line, htmk::button_help_path, "bt", id_html.c_str(), "D", "null", htmk::utils::FixWinPath(htmk::utils::CurrentDir()).c_str(),
														disable_value.c_str());
			fileOut << line << std::endl;
			delete[] line;
			line = NULL;
		}
	}

	void MakeGui::BuildListBox(CoStr_t& CmdParam, CoStr_t& HtmlParam, FileOut_t& fileOut)
	{
		Str_t id_cmd, id_html, type_html, size_html, maxsize_html, value_html, hidden_html, position_cmd;
		Str_t default_html, disable_html, label_html, default_value, disable_value, hidden_value;
		size_t len = 0;
		char* line = NULL;

		m_file_maps.GetAttValFromFirst(CmdParam, m_file_maps.mc_att_id, id_cmd);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_id, id_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_type, type_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_size, size_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_maxsize, maxsize_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_value, value_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_hidden, hidden_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_default, default_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_disable, disable_html);
		m_file_maps.GetAttValFromFirst(HtmlParam, m_file_maps.mc_att_label, label_html);
		
		m_file_maps.GetAttValFromFirst(CmdParam, m_file_maps.mc_att_position, position_cmd);

		// <div id = "line" %s>;
		len = std::strlen(htmk::div_line_begin);
		if (hidden_html.compare(m_file_maps.mc_val_true) == 0)
		{			
			hidden_value = htmk::hidden;
			len += hidden_value.size();
		}
		len += 10;
		line = new char[len];
		std::memset(line, 0, len);
		std::sprintf(line, htmk::div_line_begin, hidden_value.c_str());
		fileOut << line << std::endl;
		delete[] line;
		line = NULL;

		// <div id = "text_label">CMD ARG 1 [Descripcion]: -fich1_datos</div>
		// div_label = "<div id = 'text_label'>CMD ARG %s [%s]: -%s</div>"
		len = std::strlen(htmk::div_label) + position_cmd.size() + label_html.size() + id_cmd.size() + 10;
		line = new char[len];
		std::memset(line, 0, len);
		std::sprintf(line, htmk::div_label, position_cmd.c_str(), label_html.c_str(), id_cmd.c_str());
		fileOut << line << std::endl;
		delete[] line;
		line = NULL;

		// <select id="html_tipo">
		// listBox_begin =  "<select id='%s' readonly>"
		len = std::strlen(htmk::listBox_begin) + id_html.size() + 10;
		line = new char[len];
		std::memset(line, 0, len);
		std::sprintf(line, htmk::listBox_begin, id_html.c_str());
		fileOut << line << std::endl;
		delete[] line;
		line = NULL;

		// <option value="S">Estructura con Separador</option>
		// listBox_opcion = "<option value='%s' %s %s>%s</option>"
		// listBox_opcion = "<option value='VALOR' selected="selected" disabled="disabled">Descripcion</option>"
		MapSS_t listValues;
		ExtractValuesOfListBox(value_html, listValues);
		for(MapSS_t::iterator it = listValues.begin(); it != listValues.end(); it++)
		{			
			default_value.clear();
			disable_value.clear();
			len = std::strlen(htmk::listBox_opcion) + it->first.size() + it->second.size();
			if (default_html.compare(it->first) == 0)
			{
				default_value = htmk::select_default;
				len += default_value.size();				
			}
			if (disable_html.compare(m_file_maps.mc_val_true) == 0)
			{
				disable_value = htmk::disable;
				len += disable_value.size();				
			}
			len += 10;
			line = new char[len];
			std::memset(line, 0, len);
			std::sprintf(line, htmk::listBox_opcion, it->first.c_str(), default_value.c_str() , disable_value.c_str() , it->second.c_str());
			fileOut << line << std::endl;
			delete[] line;
			line = NULL;		
		}

		//</select> 	
		fileOut << htmk::listBox_end;

	}

	void MakeGui::ExtractValuesOfListBox(CoStr_t& raw, MapSS_t& values)
	{
		VSplit_t pairs;
		htmk::utils::Split(mc_line_sep, raw.c_str(), pairs);
		//Split(mc_line_sep, raw, pairs);
		size_t pos = 0;
		for(VSplit_t::iterator it = pairs.begin(); it != pairs.end(); it++)
		{
			pos = it->find_first_of(mc_value_sep);
			values[it->substr(0, pos)] = it->substr(pos+1, it->size());
		}
	}

	inline void MakeGui::Split(CoChar_t& sep, CoStr_t& raw, VSplit_t& split_values)
	{
		Str_t line(raw);
		size_t pos = 0;
		while((pos = line.find_first_of(sep)) != std::string::npos)
		{
			split_values.push_back(line.substr(0, pos));
			line = line.substr(pos+1, line.size());
		}
		split_values.push_back(line);
	}

	bool MakeGui::buildHtml()
	{
		Str_t root_folder, name_of_default_page, root_html_path;
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_folder, root_folder);
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_name_of_default_page, name_of_default_page);
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_html_path, root_html_path);

		Buffer_t cmd;
		//cmd << "mkdir \"" << root_html_path << root_folder << "\" > " << mc_fn_results << " 2> " << mc_fn_errors;
        cmd << "mkdir \"" << root_html_path << root_folder << "\" 2> " << mc_fn_errors;

		if (!ExecuteSystemCmd(cmd.str())) return false;
		cmd.str("");

		cmd << root_html_path << root_folder << m_dir_sep << name_of_default_page;
		m_fileOut.open(cmd.str().c_str());
		if (m_fileOut.is_open())
		{
			VCmdArgsSorted_t CmdArgsSorted;
			sortArgs(m_file_maps.GetAllParams(), CmdArgsSorted);
			m_fileOut << htmk::html_begin;

			Str_t type_html, htmlParam;		
			for(VCmdArgsSorted_t::iterator it = CmdArgsSorted.begin(); it != CmdArgsSorted.end(); it++)
			{							
				m_file_maps.GetPairSecond(it->arg, htmlParam);
				m_file_maps.GetAttValFromFirst(htmlParam, m_file_maps.mc_att_type, type_html);			
				if (type_html.compare("text") == 0 || type_html.compare("file") == 0)
					BuildInputs(it->arg, htmlParam, m_fileOut);
				else if(type_html.compare("select") == 0)
					BuildListBox(it->arg, htmlParam, m_fileOut);

				m_fileOut << htmk::div_end; // </div>
			}

			m_fileOut << htmk::html_end;
			m_fileOut.close();
		}
		else return false;

		return true;
	}

	bool MakeGui::buildJavaScript()
	{
		Str_t root_folder, root_html_path;
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_folder, root_folder);		
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_html_path, root_html_path);

		Buffer_t cmd;
		//cmd << "mkdir \"" << root_html_path << root_folder << m_dir_sep << "javascript" << "\" > " << mc_fn_results << " 2> " << mc_fn_errors;
        cmd << "mkdir \"" << root_html_path << root_folder << m_dir_sep << "javascript" << "\" 2> " << mc_fn_errors;
		if (!ExecuteSystemCmd(cmd.str())) return false;
		cmd.str("");
		size_t len = 0;
		char* line = NULL;

		cmd << root_html_path << root_folder << m_dir_sep << htmk::js_rel_path;
		m_fileOut.open(cmd.str().c_str());
		if (m_fileOut.is_open())
		{
			m_fileOut << htmk::js_globals;
			m_fileOut << htmk::js_GetSelectValueFromListBox;	
			m_fileOut << htmk::js_hiddeObjs;
			m_fileOut << htmk::js_javaGetFullPath;
			m_fileOut << htmk::js_ExecuteCMD;
			m_fileOut << htmk::js_filebrowser;
			m_fileOut << htmk::js_getfileinfo;
			m_fileOut << htmk::js_getlistfiles;
            m_fileOut << htmk::js_INITfilebrowser;
			m_fileOut << htmk::js_StringtoXml;
			m_fileOut << htmk::js_fixWinPath;
			m_fileOut << htmk::js_checkAjaxResponse;
			m_fileOut << htmk::js_printFiles;

			VCmdArgsSorted_t CmdArgsSorted;
			sortArgs(m_file_maps.GetAllParams(), CmdArgsSorted);
			m_fileOut << htmk::js_BuildParams_begin << std::endl; // function BuildParams() {

			Str_t type_html, htmlParam, id_html;
			for(VCmdArgsSorted_t::iterator it = CmdArgsSorted.begin(); it != CmdArgsSorted.end(); it++)
			{
				m_file_maps.GetPairSecond(it->arg, htmlParam);
				m_file_maps.GetAttValFromFirst(htmlParam, m_file_maps.mc_att_type, type_html);
				m_file_maps.GetAttValFromFirst(htmlParam, m_file_maps.mc_att_id, id_html);
				if (it == CmdArgsSorted.begin())
				{
					if (type_html.compare("text") == 0 || type_html.compare("file") == 0)
					{
						// var params = "html_fich1_datos=" + document.getElementById("html_fich1_datos").value;
						// js_BuildParams_add_first_input = "var params = '%s' + document.getElementById('%s').value;"
						len = std::strlen(htmk::js_BuildParams_add_first_input) + id_html.size() + id_html.size() + 10;
						line = new char[len];
						std::memset(line, 0, len);
						std::sprintf(line, htmk::js_BuildParams_add_first_input, id_html.c_str(), id_html.c_str());
						m_fileOut << line << std::endl;
						delete[] line;
						line = NULL;
					}
					else if(type_html.compare("select") == 0)					
					{
						// var params = "html_fich1_datos=" + GetSelectValueFromListBox("html_fich1_datos");
						// js_BuildParams_add_first_listBox = "var params = '%s' + GetSelectValueFromListBox('%s');"
						len = std::strlen(htmk::js_BuildParams_add_first_listBox) + id_html.size() + id_html.size() + 10;
						line = new char[len];
						std::memset(line, 0, len);
						std::sprintf(line, htmk::js_BuildParams_add_first_listBox, id_html.c_str(), id_html.c_str());
						m_fileOut << line << std::endl;
						delete[] line;
						line = NULL;
					}
				}
				else
				{
					if (type_html.compare("text") == 0 || type_html.compare("file") == 0)
					{
						// params += "&" + "html_fich1_struc=" + document.getElementById("html_fich1_struc").value
						// js_BuildParams_add_input = "params += '&' + '%s' + document.getElementById('%s').value;";
						len = std::strlen(htmk::js_BuildParams_add_input) + id_html.size() + id_html.size() + 10;
						line = new char[len];
						std::memset(line, 0, len);
						std::sprintf(line, htmk::js_BuildParams_add_input, id_html.c_str(), id_html.c_str());
						m_fileOut << line << std::endl;
						delete[] line;
						line = NULL;
					}
					else if(type_html.compare("select") == 0)					
					{
						// params += "&" + "html_tipo=" + GetSelectValueFromListBox("html_tipo");
						// js_BuildParams_add_listBox = "params += '&' + '%s' + GetSelectValueFromListBox('%s');";
						len = std::strlen(htmk::js_BuildParams_add_listBox) + id_html.size() + id_html.size() + 10;
						line = new char[len];
						std::memset(line, 0, len);
						std::sprintf(line, htmk::js_BuildParams_add_listBox, id_html.c_str(), id_html.c_str());
						m_fileOut << line << std::endl;
						delete[] line;
						line = NULL;
					}
				}
			}

			m_fileOut << htmk::js_BuildParams_end;
			m_fileOut.close();
		} else return false;

		return true;
	}

	bool MakeGui::buildCSS()
	{
		Str_t root_folder, root_html_path;
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_folder, root_folder);		
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_html_path, root_html_path);

		Buffer_t cmd;
		//cmd << "mkdir \"" << root_html_path << root_folder << m_dir_sep << "css" << "\" > " << mc_fn_results << " 2> " << mc_fn_errors;
        cmd << "mkdir \"" << root_html_path << root_folder << m_dir_sep << "css" << "\" 2> " << mc_fn_errors;

		if (!ExecuteSystemCmd(cmd.str())) return false;
		cmd.str("");

		cmd << root_html_path << root_folder << m_dir_sep << htmk::css_rel_path;
		m_fileOut.open(cmd.str().c_str());
		if (m_fileOut.is_open())
		{
			m_fileOut << htmk::css;
			m_fileOut.close();
		} else return false;

		return true;
	}

	bool MakeGui::buildJava()
	{
		Str_t root_folder, root_html_path;
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_folder, root_folder);		
		m_file_config.GetAttValFromFirst(m_file_config.mc_et_HtmlGui, m_file_config.mc_att_root_html_path, root_html_path);

		Buffer_t cmd;
		//cmd << "mkdir \"" << root_html_path << root_folder << m_dir_sep << "appletUtils" << "\" > " << mc_fn_results << " 2> " << mc_fn_errors;
        cmd << "mkdir \"" << root_html_path << root_folder << m_dir_sep << "appletUtils" << "\" 2> " << mc_fn_errors;
		if (!ExecuteSystemCmd(cmd.str())) return false;
					
		cmd.str("");						
#ifdef WIN
		Str_t CurrenDir = htmk::utils::DirLevel(htmk::utils::NumOfDirLevels()-1);
		CoCStr_t copy = "copy \"";
#endif
#ifdef LINUX
		Str_t CurrenDir = htmk::utils::DirLevel(htmk::utils::NumOfDirLevels()-1);
		CoCStr_t copy = "cp \"";
#endif		
#ifdef APPLE
		Str_t CurrenDir = htmk::utils::DirLevel(htmk::utils::NumOfDirLevels()-1);
		CoCStr_t copy = "cp \"";
#endif		

		cmd << copy << CurrenDir << m_dir_sep << "GUI_TEMPLATE" << m_dir_sep << "appletUtils" << m_dir_sep << "FileHelper_Signed.jar\" \"" << root_html_path << /*root_folder << m_dir_sep << "appletUtils\" > " << mc_fn_results << " 2> " << mc_fn_errors*/ root_folder << m_dir_sep << "appletUtils\" 2> " << mc_fn_errors;

		if (!ExecuteSystemCmd(cmd.str())) return false;

		cmd.str("");				
		cmd << copy << CurrenDir << m_dir_sep << "GUI_TEMPLATE" << m_dir_sep << "appletUtils" << m_dir_sep << "FileHelper.class\" \"" << root_html_path <</*root_folder << m_dir_sep << "appletUtils\" > " << mc_fn_results << " 2> " << mc_fn_errors;*/root_folder << m_dir_sep << "appletUtils\" 2> " << mc_fn_errors;
		if (!ExecuteSystemCmd(cmd.str())) return false;

		return true;
	}
}

