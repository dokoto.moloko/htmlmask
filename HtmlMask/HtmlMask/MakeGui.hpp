#ifndef MAKEGUI_HPP
#define MAKEGUI_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#ifdef WIN
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include <cstdio>
#include <cstring>

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "mapAttribs.hpp"
#include "configAttribs.hpp"
#include "GuiConsts.hpp"
#include "Utils.hpp"

namespace htmk 
{
	class MakeGui
	{
    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/ 
	private:
		typedef const std::string                               CoStr_t;  		
		typedef std::string										Str_t;  		
		typedef char*		                                    CStr_t; 
		typedef const char*		                                CoCStr_t;
		typedef const char		                                CoChar_t;
        typedef htmk::mapAttribs								MapConfig_t;
		typedef htmk::configAttribs								Config_t;
        typedef std::ofstream                                   FileOut_t;
		typedef std::ifstream                                   FileIn_t;
		typedef std::stringstream								Buffer_t;
		typedef std::map<Str_t, Str_t>							MapSS_t;
		typedef struct											CmdArgsSorted_t
		{
				size_t										position;
				Str_t										arg;
		}														CmdArgsSorted_t;
		typedef std::vector<CmdArgsSorted_t>					VCmdArgsSorted_t;
		typedef std::vector<Str_t>								VSplit_t;

    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/ 
	private:
        Config_t												m_file_config;
        MapConfig_t								                m_file_maps;
		FileOut_t												m_fileOut;
		FileIn_t		                                        m_fileInput;
		
		CoCStr_t												mc_fn_errors;
		CoCStr_t												mc_fn_results;
		CoCStr_t												mc_fn_cmd;
		CoCStr_t												mc_unix_sep;
		CoCStr_t												mc_win_sep;
		
		CoChar_t												mc_line_sep;
		CoChar_t												mc_value_sep;
		
		CoCStr_t												mc_conf_path;
		CoCStr_t												mc_map_path;
		
		Str_t													m_dir_sep;

	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE METHODS
    *--------------------------------------------------------------------------------------------------------------*/ 
	private:
		bool					buildHtml						();
		bool					buildJavaScript					();
		bool					buildCSS						();
		bool					buildJava						();
		bool					ExecuteSystemCmd				(CoStr_t& cmd);
		bool					readTextFile					(CoStr_t& path, Str_t& file);
		bool					setDirSep						(CoStr_t& path);
		friend bool				FuncSort1						(const CmdArgsSorted_t& left, const CmdArgsSorted_t& right);
		void					sortArgs						(const MapSS_t& data, VCmdArgsSorted_t& vArgs);
		void					BuildInputs						(CoStr_t& CmdParam, CoStr_t& HtmlParam, FileOut_t& fileOut);
		void					BuildListBox					(CoStr_t& CmdParam, CoStr_t& HtmlParam, FileOut_t& fileOut);
		void					ExtractValuesOfListBox			(CoStr_t& raw, MapSS_t& values);
		void					Split							(CoChar_t& sep, CoStr_t& raw, VSplit_t& split_values); // DEPRECATED (Use: htmk::Utils::Split)
		Str_t					FixWinPath						(CoStr_t& path); // DEPRECATED (Use htmk::Utils::FixWinPath)

	/*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/ 
	public:
                                
								MakeGui							(CoCStr_t conf_path, CoCStr_t confmap_path);
								~MakeGui						(void);
		bool					Generate						();
	};
}
#endif // MAKEGUI_HPP


