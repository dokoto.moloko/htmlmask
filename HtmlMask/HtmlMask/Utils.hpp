#ifndef UTILS_HPP
#define UTILS_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#ifdef WIN
	#define _CRT_SECURE_NO_WARNINGS
	//#include <windows.h>	
	#include "atlbase.h"
	#include "atlstr.h"
	#include "comutil.h"
	#include "Strsafe.h"
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <dirent.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <unistd.h>
    #define GetCurrentDir getcwd
 #endif

#include <cstdio>
#include <cstring>

#include <string>
#include <vector>

namespace htmk 
{
	namespace utils 
	{
		/*--------------------------------------------------------------------------------------------------------------
		* TYPES
		*---------------------------------------------------------------------------------------------------------------
		* 'Co'			: const
		* 'CStr'		: char*
		* 'Str'			: std::string
		* 'VecStr'		: std::vector<std::string>
		*--------------------------------------------------------------------------------------------------------------*/		
		typedef std::string										Str_t;  		
		typedef char*		                                    CStr_t; 
		typedef char											Char_t;
		typedef const char*		                                CoCStr_t;
		typedef const char		                                CoChar_t;
		typedef const std::string								CoStr_t;  		
		typedef std::vector<std::string>						VecStr_t;
		typedef const std::vector<std::string>					CoVecStr_t;
		typedef size_t											Int_t;
		typedef const int										CInt_t;
		typedef struct											
		{
						Str_t							name;
						Str_t							ruta;
						Str_t							tipo;
		}														file_t;
		typedef std::vector<file_t>								Vfile_t;
		typedef const std::vector<file_t>						CoVfile_t;

		/*--------------------------------------------------------------------------------------------------------------
		* FUNCTIONS
		*--------------------------------------------------------------------------------------------------------------*/		
		bool					Split							(CoChar_t& sep, CoCStr_t raw, VecStr_t& split_values);
		Str_t					CurrentDir						(void);
		size_t					NumOfDirLevels					(void);
        size_t					NumOfDirLevels					(CoCStr_t& path);
		Char_t					GetDirSep						(void);

		/* DIRLEVEL(CInt_t& Level, Str_t& Dir)
		* Si tenemos el Directorio C:\uno\dos\tres\cuatro y queremos obtener la ruta hasta C:\uno\dos
		* le pasamos (N�NIVELES - 2), es decir, dos niveles arriba
		* Ej:
		* Str_t& Dir;
		* DirLevel(NumOfDirLevels() - 2, Dir);
		*/
		Str_t					DirLevel						(CInt_t& Level, CoStr_t& path);		
		Str_t					DirLevel						(CInt_t& Level);
		Str_t					FixWinPath						(CoStr_t& path);	
		Str_t					ExtractEXTENSION				(CoStr_t& path);
		bool					ListFiles						(CoCStr_t path, CoChar_t tipo, Vfile_t& files);		
#ifdef WIN
		bool					ListFilesWIN					(CoCStr_t path, Vfile_t& files);
#else
        bool					ListFilesUNIX					(CoCStr_t path, Vfile_t& files);
#endif		
	}
}

#endif //UTILS_HPP

