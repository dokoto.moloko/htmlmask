#include "XmlConfig.hpp"

namespace htmk 
{
	XmlConfig::XmlConfig(void)
	{
	}

	XmlConfig::~XmlConfig(void)
	{
	}
	
	bool XmlConfig::Load(CoStr_t& )
	{
		return true;
	}
		
	bool XmlConfig::Load(CoCStr_t path)
	{
		return Load(const_cast<CStr_t>(path));
	}
		

	bool XmlConfig::loadXML(CoStr_t& , PairsOfFields_t& , FieldsAttribs_t& )
	{
		return true;
	}

	bool XmlConfig::loadXML(CoStr_t& , FieldsAttribs_t& )
	{
		return true;				
	}
	
	inline bool XmlConfig::GetPairSecond(CoStr_t& First, Str_t& Second)
	{
		if (m_PairsOfFields.find(First) != m_PairsOfFields.end())
			Second = m_PairsOfFields.find(First)->second;
		else
			return false;

		return true;
	}

	inline bool XmlConfig::GetPairFirst(CoStr_t& Second, Str_t& First)
	{
		PairsOfFields_t::iterator it;
		for(it = m_PairsOfFields.begin(); it != m_PairsOfFields.end() && it->second.compare(Second) != 0; it++) {}
		if (it == m_PairsOfFields.end())
			return false;
		else
			First = it->first;

		return true;
	}

	inline bool XmlConfig::GetAllAttFromFirst(CoStr_t& First, MapSS_t& Attrib)
	{
		Attrib.insert(m_FieldsAttribs[First].begin(), m_FieldsAttribs[First].end());
		return (Attrib.size() == 0)? false : true;
	}

	inline bool XmlConfig::GetAllAttFromSecond(CoStr_t& Second, MapSS_t& Attribs)
	{
		Str_t First;
		if (GetPairFirst(Second, First) == false) return false;
		return GetAllAttFromFirst(First, Attribs);		
	}

	bool XmlConfig::GetAttValFromFirst(CoStr_t& First, const Str_t& Attrib, Str_t& value)
	{
		value = m_FieldsAttribs[First][Attrib];
		return (value.size() == 0)? false : true;
	}

	bool XmlConfig::GetAttValFromSecond(CoStr_t& Second, CoStr_t& Attrib, Str_t& value)
	{
		Str_t First;
		if (GetPairFirst(Second, First) == false) return false;
		return GetAttValFromFirst(First, Attrib, value);
	}
}
