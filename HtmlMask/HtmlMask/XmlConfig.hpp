#ifndef XMLCONFIG_HPP
#define XMLCONFIG_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include <cstdio>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include "pugixml.hpp"

namespace htmk 
{
	class XmlConfig
	{
	/*--------------------------------------------------------------------------------------------------------------
    * PROTECTED TYPES
    *--------------------------------------------------------------------------------------------------------------*/         
    protected:
		typedef std::string										Str_t;  		
		typedef char*		                                    CStr_t; 
		typedef char											Char_t;
		typedef const CStr_t	                                CoCStr_t;
		typedef const Char_t	                                CoChar_t;
		typedef const Str_t										CoStr_t;  
		
		typedef std::map<Str_t, Str_t>							MapSS_t;		
		typedef std::map<Str_t, MapSS_t>						MapOfMapsSS_t;

		typedef MapSS_t											PairsOfFields_t;
		typedef MapOfMapsSS_t									FieldsAttribs_t;
		typedef pugi::xml_document								XmlDoc_t;
		typedef pugi::xml_node									Node_t;
		typedef pugi::xml_attribute								Attrib_t;

    /*--------------------------------------------------------------------------------------------------------------
    * PROTECTED ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/
	protected:
		PairsOfFields_t											m_PairsOfFields;
		FieldsAttribs_t											m_FieldsAttribs;
		XmlDoc_t												m_xml_config;
	
    /*--------------------------------------------------------------------------------------------------------------
    * PROTECTED METHODS
	* (FROM LEFT MARGIN) : 2 TABS(returning) - 5 TABS(METHOD NAME) - 7 TABS(PARAMETERS)
    *--------------------------------------------------------------------------------------------------------------*/
	protected:		
		virtual bool					loadXML						(CoStr_t& path, PairsOfFields_t& PairsOfFields, FieldsAttribs_t& FieldsAttribs);
		virtual bool					loadXML						(CoStr_t& path, FieldsAttribs_t& FieldsAttribs);

    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/
/*
	EXPLICAION DE LOS MAPEOS CONFIG.XML
	------------------------------------------------------------------------------------

	FieldsAttribs.first = TermianlProgram or HtmlGui
	FieldsAttribs.first.first = Nombre del Atributo (ej: listen_port, name_of_default_page, root_html_path, etc..)
	FieldsAttribs.first.second = Valor del Atributo (ej: '4444', 'index.html', '/var/www/', etc..)

	EXPLICAION DE LOS MAPEOS MAPS.XML
	------------------------------------------------------------------------------------
	PairsOfFields.first = CommandArgument (ej: fich1_datos)
	PairsOfFields.second = HtmlArgument   (ej: html_fich1_datos)

	FieldsAttribs.first = CommandArgument (ej: fich1_datos)
	FieldsAttribs.first.first = Nombre del Atributo (ej: format, value, size, etc..)
	FieldsAttribs.first.second = Valor del Atributo (ej: '-%s "%s"', '', '60', etc..)
*/

	public:
										XmlConfig						(void);
										virtual ~XmlConfig				(void);
		virtual bool					Load							(CoStr_t& path);
		virtual bool					Load							(CoCStr_t path);
		virtual bool					GetPairSecond					(CoStr_t& First, Str_t& Second);
		virtual bool					GetPairFirst					(CoStr_t& Second, Str_t& First);
		virtual bool					GetAllAttFromFirst				(CoStr_t& First, MapSS_t& Attribs);
		virtual bool					GetAllAttFromSecond				(CoStr_t& Second, MapSS_t& Attribs);
		virtual bool					GetAttValFromFirst				(CoStr_t& First, CoStr_t& Attrib, Str_t& value);
		virtual bool					GetAttValFromSecond				(CoStr_t& Second, CoStr_t& Attrib, Str_t& value);
		virtual bool					IsEmpty							() const { return (m_FieldsAttribs.size() == 0)? true : false; }
		virtual const MapSS_t&			GetAllParams					() const { return m_PairsOfFields; }


	};
}
#endif //XMLCONFIG_HPP
