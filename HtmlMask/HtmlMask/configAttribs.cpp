#include "configAttribs.hpp"

namespace htmk 
{
	configAttribs::configAttribs(void) :

		mc_et_Config("Config"),
		mc_et_HtmlGui("HtmlGui"),
		mc_et_TermianlProgram("TerminalProgram"),
		mc_att_fullPath("fullPath"),
		mc_att_listen_port("listen_port"),
		mc_att_root_folder("root_folder"),
		mc_att_name_of_default_page("name_of_default_page"),
		mc_att_root_html_path("root_html_path")

	{
		mc_tags.insert(mc_et_Config);
		mc_tags.insert(mc_et_HtmlGui);
		mc_tags.insert(mc_et_TermianlProgram);
		mc_attribs.insert(mc_att_fullPath);
		mc_attribs.insert(mc_att_listen_port);
		mc_attribs.insert(mc_att_root_folder);
		mc_attribs.insert(mc_att_name_of_default_page);
		mc_attribs.insert(mc_att_root_html_path);
	}

	configAttribs::~configAttribs(void)
	{
	}

	bool configAttribs::Load(const Str_t& path)
	{
		return loadXML(path, m_FieldsAttribs);
	}

/*
	EXPLICAION DE LOS MAPEOS CONFIG.XML
	------------------------------------------------------------------------------------

	FieldsAttribs.first = CommandArgument or HtmlArgument (ej: 'fich1_datos' or 'html_fich1_datos')
	FieldsAttribs.first.first = Nombre del Atributo (ej: listen_port, name_of_default_page, root_html_path, etc..)
	FieldsAttribs.first.second = Valor del Atributo (ej: '4444', 'index.html', '/var/www/', etc..)
*/
	bool configAttribs::loadXML(CoStr_t& path, FieldsAttribs_t& FieldsAttribs)
	{		
		pugi::xml_parse_result result = m_xml_config.load_file(path.c_str());		
		if (!result) 
		{
		  std::cerr << "XML [" << path << "] parsed with errors." << std::endl;
		  std::cerr << "Error description: " << result.description() << std::endl;		  		  
		  return false;
		}		

		Node_t Cmd = m_xml_config.child(mc_et_Config).child(mc_et_TermianlProgram);			
		for (pugi::xml_attribute attr = Cmd.first_attribute(); attr; attr = attr.next_attribute())
			FieldsAttribs[Str_t(Cmd.name())][Str_t(attr.name())] = Str_t(attr.value());
			
		Node_t Html = m_xml_config.child(mc_et_Config).child(mc_et_HtmlGui);
		for (Attrib_t attr = Html.first_attribute(); attr; attr = attr.next_attribute())
			FieldsAttribs[Str_t(Html.name())][Str_t(attr.name())] = Str_t(attr.value());											

		return true;
	}
}
