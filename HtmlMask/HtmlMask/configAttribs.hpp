#ifndef CONFIGATTRIBS_HPP
#define CONFIGATTRIBS_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include <iostream>
#include <set>
#include "pugixml.hpp"
#include "XmlConfig.hpp"

namespace htmk 
{
	class configAttribs : public XmlConfig
	{
	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/         
    private:
		typedef const char*											CCStr_t; 
		typedef std::set<CCStr_t>									SetCCS_t;

    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC CONST ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/
	public:
		SetCCS_t													mc_tags;
		SetCCS_t													mc_attribs;
		CCStr_t														mc_et_Config;
		CCStr_t														mc_et_HtmlGui;
		CCStr_t														mc_et_TermianlProgram;
		CCStr_t														mc_att_fullPath;
		CCStr_t														mc_att_listen_port;
		CCStr_t														mc_att_root_folder;
		CCStr_t														mc_att_name_of_default_page;
		CCStr_t														mc_att_root_html_path;
	
    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE METHODS
	* (FROM LEFT MARGIN) : 2 TABS(returning) - 5 TABS(METHOD NAME) - 7 TABS(PARAMETERS)
    *--------------------------------------------------------------------------------------------------------------*/
	private:		
		bool					loadXML							(const Str_t& path, FieldsAttribs_t& FieldsAttribs);

    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/
	public:
								configAttribs					(void);
								~configAttribs					(void);
		bool					Load							(CoStr_t& path);


	};
}
#endif //CONFIGATTRIBS_HPP
