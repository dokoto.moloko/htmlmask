#ifndef CREATEFILEXML_HPP
#define CREATEFILEXML_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include <iostream>
#include <set>
#include "pugixml.hpp"
#include "XmlConfig.hpp"
#include "Utils.hpp"

namespace htmk 
{
	class createFileXml
	{
	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/         
    private:		
		htmk::utils::Vfile_t										VecStr;		
		pugi::xml_document											XmlDoc;
		
    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/
	public:
								createFileXml					(htmk::utils::Vfile_t lVecStr);
								~createFileXml					(void);
		bool					Conv							(void);
		htmk::utils::Str_t		ToString						(void);

	};
}

#endif