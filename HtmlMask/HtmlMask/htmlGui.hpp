#ifndef HTMLGUI_HPP
#define HTMLGUI_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#ifdef WIN	
	#define _CRT_SECURE_NO_WARNINGS
#endif
#include <cstdio>
#include <cstring>

#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

#include "webServer.hpp"
#include "mapAttribs.hpp"
#include "configAttribs.hpp"
#include "createFileXml.hpp"
#include "Utils.hpp"

namespace htmk 
{

    class htmlGui : public webServer
    {
    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC TYPES
    *--------------------------------------------------------------------------------------------------------------*/    
    public:
             
    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/         
    private:
		typedef std::string                                     Str_t;  		
		typedef char*		                                    CStr_t; 
		typedef char											Char_t;
		typedef const char*		                                CCStr_t;
		typedef std::map<Str_t, Str_t>							MapSS_t;		
		typedef std::map<Str_t, bool>							MapSB_t;
        typedef htmk::mapAttribs								MapConfig_t;
		typedef htmk::configAttribs								Config_t;
		typedef struct											Connection_info_struct_t
		{	
				int											connectiontype;
				Str_t										answerstring;				
				struct MHD_PostProcessor*					postprocessor;
				MapSS_t										post_key_value;

		}														Connection_info_struct_t;
		typedef struct											CmdArgsSorted_t
		{
				size_t										position;
				Str_t										arg;
		}														CmdArgsSorted_t;
		typedef std::vector<CmdArgsSorted_t>					VCmdArgsSorted_t;
		typedef std::vector<std::string>						VStr;
        typedef std::ifstream                                   File_t;
		typedef std::stringstream								Buffer_t;	
             
    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/  
    private:
        static Str_t		                                    m_localhost;
        static File_t                                           m_fileInput;
        static Config_t											m_file_config;
        static MapConfig_t	                                    m_file_maps;		
		static MapSB_t											m_isBinaryFile;
		static Str_t											m_dir_sep;
		static htmk::mapAttribs*								m_file_maps2;	
		static Str_t											m_result_file_log;
		static Str_t											m_result_file_errors;
		static CCStr_t											m_error_page;		
		static CCStr_t											m_unix_sep;
		static CCStr_t											m_win_sep;
		static CCStr_t											m_win_dev_sep;
		static CCStr_t											m_url_sep;
		static CCStr_t											m_jar;
		static CCStr_t											m_css;
		static CCStr_t											m_html;
		static CCStr_t											m_htm;
		static CCStr_t											m_js;
		static CCStr_t											m_post;
		static CCStr_t											m_get;																
		static const size_t										m_post_buffer_size;
		static const size_t										m_max_name_size;
		static const size_t										m_max_answer_size;
																enum {GET, POST};
		static MapSS_t											m_mimetypes;
        enum                                                    {EXECUTE, LIST_FILES, SEL_ERROR};
	
        
        
    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE METHODS
	* (FROM LEFT MARGIN) : 2 TABS(returning) - 5 TABS(METHOD NAME) - 7 TABS(PARAMETERS)
    *--------------------------------------------------------------------------------------------------------------*/         
    private:                    
        static int              on_client_connect               (void* cls, const sockaddr* addr, socklen_t addrlen);
        static int              answer_to_connection            (void* cls, struct MHD_Connection* connection, const char* url, 
                                                                 const char* method, const char* version, const char* upload_data, 
                                                                 size_t* upload_data_size, void** con_cls);  
        static void             request_completed               (void* cls, struct MHD_Connection* connection, void** con_cls, 
                                                                 enum MHD_RequestTerminationCode toe);  
		static int				iterate_post					(void *coninfo_cls, enum MHD_ValueKind kind, const char *key,
																 const char *filename, const char *content_type, const char *transfer_encoding,
																 const char *data, uint64_t off, size_t size);
		static void				page_dispatcher					(const Str_t& page_request, Str_t& page_return, Str_t& mimetype);

		static bool				readTextFile					(const Str_t& path, Str_t& file);
		static bool				readBinaryFile					(const Str_t& path, Str_t& file);
		static int				send_page						(struct MHD_Connection *connection, const Str_t& page, const Str_t& mimetype);
		static void				ExecuteCmd						(const MapSS_t& data, Str_t& send_page);
		static void				ExecuteSystemCmd				(const Str_t& cmd);
        static void             openBrowser                     ();
		static bool				setDirSep						(const Str_t& path);		
		static Str_t			FixWinPath						(const Str_t& path); // DEPRECATED (Use htmk::Utils::FixWinPath)
		friend bool				FuncSort1						(const CmdArgsSorted_t& left, const CmdArgsSorted_t& right);
		static void				sortArgs						(const MapSS_t& data, VCmdArgsSorted_t& vArgs);
		static int				FuncSelector					(MapSS_t post_key_value);
		static bool				listFiles						(htmk::utils::CoCStr_t path, htmk::utils::CoChar_t tipo, htmk::utils::Vfile_t& files);
		static bool				Files2XML						(htmk::utils::Vfile_t& file, Str_t xmlResponse);
		
    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/    
    public:
        
                                htmlGui                         (const Str_t &conf_path, const Str_t &confmap_path, const int port = 8888);
                                ~htmlGui                        ();
        bool                    Run                             ();
		
    };	 
}



#endif // HTMLGUI_HPP

