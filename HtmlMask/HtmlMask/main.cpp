#include "main.hpp"

int main (const int argc, const char* argv[])
{
	Main main;
	if (main.CheckArgs(argc, argv) == false) 
	{
#ifdef WIN		
		std::cout << std::endl << "Pulse cualquier tecla para terminar..." << std::endl;
		std::cin.get();
#endif
		return false;
	}
	return main.Run();	
}


/*--------------------------------------------------------------------------------------------------------------
 * Main::Main
 *--------------------------------------------------------------------------------------------------------------*/

Main::Main(void) :
m_obg(2)
{	
}

Main::~Main(void)
{
}

bool Main::Run(void)
{	
	if (m_args.find("-create_html") != m_args.end())
	{
		htmk::MakeGui mkGui(m_args["-config"].c_str(), m_args["-maps"].c_str());
		return mkGui.Generate();
	}
	else
	{
		htmk::htmlGui Gui(m_args["-config"], m_args["-maps"]);
		return Gui.Run();		
	}
	return 1;
}

bool Main::CheckArgs(const int argc, const char* argv[])
{
	int oblg = 0;
	if (CargaArgs(argc, argv) == false) return false;
    
    for ( Args_t::iterator it = m_args.begin(); it != m_args.end(); it++)
    {
        if (((*it).first).compare("-config") == 0)
		{
			if (Validacion1((*it).second) == false) return false;
			oblg++;         
		}
        else if (((*it).first).compare("-maps") == 0)        
		{
			if (Validacion1((*it).second) == false) return false;
            oblg++;
		}
        else if (((*it).first).compare("-open_browser") == 0)
		{
			if (Validacion2() == false) return false;				
            continue;
		}
        else if (((*it).first).compare("-create_html") == 0)
		{
			if (Validacion3() == false) return false;		
            continue;
		}
        else if (((*it).first).compare("-help") == 0)
		{
			std::cerr <<  m_use << std::endl;
            return false;
		}
		else if (((*it).first).compare("prog") == 0)
			continue;
        else
		{
            std::cerr <<  "Parametros no reconocido. Use --help." << std::endl;
            return false;
		}
    }	

	if (oblg != m_obg)
	{
		std::cerr <<  "Faltan parametros obligatorios. Use --help." << std::endl;
        return false;
	}
    
	return true;
}


bool Main::CargaArgs(const int argc, const char *argv[])
{
	std::string prog_name(argv[0]);
	std::string like_unix = prog_name.substr(0, 1);
    std::string like_win  = prog_name.substr(1, 2);
	
    if ( like_unix.compare("/") == 0 )
		prog_name.substr(prog_name.find_last_of("/")+1, prog_name.size());
	else if ( like_win.compare(":\\") == 0 ) 
		prog_name = prog_name.substr(prog_name.find_last_of("\\")+1, prog_name.size());
	
    
    size_t pos = 0;
    while ((pos = m_use.find("%s")) != std::string::npos)
		m_use.replace(pos, 2, prog_name);
    
    if (argc == 1)
    {
		std::cerr <<  m_use << std::endl;
		return false;
    }
    
	for (size_t i = 0; i < SC(size_t, argc-1);)
    {
        if (i == 0)
        {
            m_args["prog"] = std::string(argv[i]);
            i++;
        } else
        {
			m_args[std::string(argv[i])] = std::string(argv[i+1]);
            i += 2;
        }
    } 
    
	return true;
}

bool Main::Validacion1(const std::string& valor)
{
	std::ifstream inFile(valor.c_str());
	if (inFile.fail())
	{
		std::stringstream ss;
		ss << "El fichero no parece existir en : " << valor << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}
    
	inFile.close();
    
	return true;
}

bool Main::Validacion2()
{
	if (m_args.find("-create_html") != m_args.end())
	{
		std::stringstream ss;
		ss << "No es posible usar el  '-open_browser' con el argumento '-create_html', son excluyentes." << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}
    
	return true;
}

bool Main::Validacion3()
{
	if (m_args.find("-open_browser") != m_args.end())
	{
		std::stringstream ss;
		ss << "No es posible usar el  '-create_html' con el argumento '-open_browser', son excluyentes." << std::endl;
		std::cerr << ss.str() << std::endl;
		return false;
	}
    
	return true;
    
	return true;
}
