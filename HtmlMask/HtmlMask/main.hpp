#ifndef MAIN_H
#define MAIN_H

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *--------------------------------------------------------------------------------------------------------------*/
#include <cassert>
#include <map>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "htmlGui.hpp"
#include "MakeGui.hpp"

/*--------------------------------------------------------------------------------------------------------------
 * MACROS
 *--------------------------------------------------------------------------------------------------------------*/
#define SC(Tipo, Valor) static_cast<Tipo>(Valor)

/*--------------------------------------------------------------------------------------------------------------
 * TYPES
 *--------------------------------------------------------------------------------------------------------------*/
typedef std::map<std::string, std::string>					Args_t;


class Main
{
    /*--------------------------------------------------------------------------------------------------------------
     * PRIVATE
     *--------------------------------------------------------------------------------------------------------------*/
private:
	Args_t						m_args;
	static std::string			m_use;
	const int					m_obg;
    
	bool						CargaArgs					(const int argn, const char* argv[]);
	bool						Validacion1					(const std::string& valor);
	bool						Validacion2					();
	bool						Validacion3					();

    /*--------------------------------------------------------------------------------------------------------------
     * PUBLIC	 
     *--------------------------------------------------------------------------------------------------------------*/
public:
    Main						(void);
    ~Main						(void);
	bool						CheckArgs					(const int argn, const char* argv[]);
	bool						Run							(void);
};

std::string Main::m_use = "Programa para interfasear un programa de consola con un GUI en HTML.\n\
Acceso desde navegador http://localhost:(listen_port)\n\n\
Linea de argumentos del programa:\n\
./HtmlMask\n\
-config       [OBLIGATORIO] = Ruta al fichero(ENTRE COMILLAS DOBLES) config.txt\n\
-maps         [OBLIGATORIO] = Ruta al fichero(ENTRE COMILLAS DOBLES) maps.txt\n\
-open_browser [OPCIONAL]    = Abre el navegador por defecto, este parametro excluye a -create_html\n\
-create_html  [OPCIONAL]    = Crea un fichero de HTML de ejemplo que soportara la interface descrita en el fichero maps.txt y\n\
\t\t\t\tconfigurada segun los criterios de config.txt .Se creara el fichero en la ruta indicada en 'root_html_path'\n\
\t\t\t\tcon el nombre indicado en 'name_of_default_page', este parametro excluye a -open_browser\n\n\
EJEMPLOS\n\
-------------------------------------------------------------------------------------------------------------------------------------\n\
Ej_1 > ./HtmlMask -config \"C:\\Tmp\\config.txt\\\" -maps \"C:\\Tmp\\maps.txt\\\" -open_browser true\n\
Ej_2 > ./HtmlMask -config \"/home/T87/config.txt\" -maps \"/home/T87/maps.txt\"\n\
Ej_3 > ./HtmlMask -config \"/home/T87/config.txt\" -maps \"/home/T87/maps.txt\" -create_html true\n\
Ej_4 > ./HtmlMask -config \"/home/T87/config.txt\" -maps \"/home/T87/maps.txt\" -create_html false\n\
-------------------------------------------------------------------------------------------------------------------------------------\n\
";

#endif

