#include "mapAttribs.hpp"

namespace htmk 
{
	mapAttribs::mapAttribs(void) :

		mc_et_Map("Map"),
		mc_et_Fields("Fields"),
		mc_et_Field("Field"),
		mc_et_CommandArgument("CommandArgument"),
		mc_et_HtmlArgument("HtmlArgument"),		
		
		mc_att_id("id"),
		mc_att_format("format"),
		mc_att_type("type"),
		mc_att_value("value"),
		mc_att_position("position"),
		mc_att_size("size"),
		mc_att_maxsize("maxsize"),
		mc_att_parameter_type("parameter_type"),
		mc_att_label("label"),
		mc_att_disable("disable"),
		mc_att_hidden("hidden"),
		mc_att_default("default"),
		
		mc_val_both("both"),
		mc_val_param("param"),
		mc_val_value("value"),
		mc_val_false("false"),
		mc_val_true("true")

	{
		mc_tags.insert(mc_et_Map);
		mc_tags.insert(mc_et_Fields);
		mc_tags.insert(mc_et_Field);
		
		mc_attribs.insert(mc_et_CommandArgument);
		mc_attribs.insert(mc_et_HtmlArgument);
		mc_attribs.insert(mc_att_id);
		mc_attribs.insert(mc_att_format);
		mc_attribs.insert(mc_att_type);
		mc_attribs.insert(mc_att_value);
		mc_attribs.insert(mc_att_position);
		mc_attribs.insert(mc_att_size);
		mc_attribs.insert(mc_att_maxsize);
		mc_attribs.insert(mc_att_hidden);
		mc_attribs.insert(mc_att_parameter_type);
		mc_attribs.insert(mc_att_label);
		mc_attribs.insert(mc_att_disable);
		mc_attribs.insert(mc_att_default);

		mc_values.insert(mc_val_both);
		mc_values.insert(mc_val_param);
		mc_values.insert(mc_val_value);
		mc_values.insert(mc_val_false);
		mc_values.insert(mc_val_true);

	}

	mapAttribs::~mapAttribs(void)
	{
	}

	bool mapAttribs::Load(const Str_t& path)
	{
		return loadXML(path, m_PairsOfFields, m_FieldsAttribs);
	}

/*
	EXPLICAION DE LOS MAPEOS MAPS.XML
	------------------------------------------------------------------------------------
	PairsOfFields.first = CommandArgument (ej: fich1_datos)
	PairsOfFields.second = HtmlArgument   (ej: html_fich1_datos)

	FieldsAttribs.first = CommandArgument (ej: fich1_datos)
	FieldsAttribs.first.first = Nombre del Atributo (ej: format, value, size, etc..)
	FieldsAttribs.first.second = Valor del Atributo (ej: '-%s "%s"', '', '60', etc..)
*/

	bool mapAttribs::loadXML(CoStr_t& path, PairsOfFields_t& PairsOfFields, FieldsAttribs_t& FieldsAttribs)
	{		
		pugi::xml_parse_result result = m_xml_config.load_file(path.c_str());		
		if (!result) 
		{
		  std::cerr << "XML [" << path << "] parsed with errors." << std::endl;
		  std::cerr << "Error description: " << result.description() << std::endl;		  
		  return false;
		}
		Node_t map = m_xml_config.child(mc_et_Map).child(mc_et_Fields);

		for (Node_t fields = map.first_child(); fields; fields = fields.next_sibling())
		{
			Node_t Cmd = fields.child(mc_et_CommandArgument);			
			for (pugi::xml_attribute attr = Cmd.first_attribute(); attr; attr = attr.next_attribute())
				FieldsAttribs[Str_t(Cmd.attribute(mc_att_id).value())][Str_t(attr.name())] = Str_t(attr.value());
			
			Node_t Html = fields.child(mc_et_HtmlArgument);
			for (Attrib_t attr = Html.first_attribute(); attr; attr = attr.next_attribute())
				FieldsAttribs[Str_t(Html.attribute(mc_att_id).value())][Str_t(attr.name())] = Str_t(attr.value());					
						
			PairsOfFields[Str_t(Cmd.attribute(mc_att_id).value())] = Str_t(Html.attribute("id").value());
		}

		return true;
	}
}


