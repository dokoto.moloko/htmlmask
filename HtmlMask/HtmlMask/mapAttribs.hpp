#ifndef MAPATTRIBS_HPP
#define MAPATTRIBS_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include <iostream>
#include <set>
#include "pugixml.hpp"
#include "XmlConfig.hpp"

namespace htmk 
{
	class mapAttribs : public XmlConfig
	{
	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/         
    private:		
		typedef const char*											CCStr_t; 
		typedef std::set<CCStr_t>									SetCCS_t;

    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC CONST ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/
	public:
		SetCCS_t													mc_tags;
		SetCCS_t													mc_attribs;
		SetCCS_t													mc_values;

		CCStr_t														mc_et_Map;
		CCStr_t														mc_et_Fields;
		CCStr_t														mc_et_Field;
		CCStr_t														mc_et_CommandArgument;
		CCStr_t														mc_et_HtmlArgument;

		CCStr_t														mc_att_id;
		CCStr_t														mc_att_format;
		CCStr_t														mc_att_type;
		CCStr_t														mc_att_value;
		CCStr_t														mc_att_position;
		CCStr_t														mc_att_size;
		CCStr_t														mc_att_maxsize;
		CCStr_t														mc_att_parameter_type;
		CCStr_t														mc_att_label;
		CCStr_t														mc_att_disable;
		CCStr_t														mc_att_hidden;
		CCStr_t														mc_att_default;

		CCStr_t														mc_val_both;
		CCStr_t														mc_val_param;
		CCStr_t														mc_val_value;
		CCStr_t														mc_val_false;
		CCStr_t														mc_val_true;

    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE METHODS
	* (FROM LEFT MARGIN) : 2 TABS(returning) - 5 TABS(METHOD NAME) - 7 TABS(PARAMETERS)
    *--------------------------------------------------------------------------------------------------------------*/
	private:		
		bool					loadXML							(CoStr_t& path, PairsOfFields_t& PairsOfFields, FieldsAttribs_t& FieldsAttribs);

    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/
	public:
								mapAttribs						(void);
								~mapAttribs						(void);
		bool					Load							(CoStr_t& path);
		size_t					NumOfArgs						() { return m_PairsOfFields.size(); }


	};
}
#endif //MAPATTRIBS_HPP
