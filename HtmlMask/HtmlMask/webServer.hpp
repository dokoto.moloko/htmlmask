#ifndef WEBSERVER_H
#define WEBSERVER_H
/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *--------------------------------------------------------------------------------------------------------------*/
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>

#ifdef WIN
	#define MHD_PLATFORM_H    
	#include <winsock2.h> // socket related definitions
	#include <ws2tcpip.h> // socket related definitions
	#include <windows.h>

	typedef SSIZE_T ssize_t; //convey Windows type to unix type
	typedef UINT64 uint64_t; //convey Windows type to unix type
	typedef UINT16 uint16_t; //convey Windows type to unix type
#endif

#if defined LINUX || defined APPLE	
	#include <sys/types.h>
	#include <sys/select.h>
	#include <sys/socket.h>
	#include <arpa/inet.h>
	#include <stdarg.h>
#endif

extern "C" 
{	        
	#include "microhttpd.h"    
}

/*--------------------------------------------------------------------------------------------------------------
 * MACROS
 *--------------------------------------------------------------------------------------------------------------*/
// CONST CHAR* TO VOID*
#define CCP_TO_VP(var) static_cast<void*>(const_cast<char*>(var))
// VOID* TO CONST CHAR*
#define VP_TO_CCP(var) static_cast<char*>(var)

namespace htmk
{
    
    class webServer
    {
    /*--------------------------------------------------------------------------------------------------------------
     * PRIVATE TYPES
     *--------------------------------------------------------------------------------------------------------------*/
    private:
        typedef struct MHD_Daemon					Daemon_t;
        
    /*--------------------------------------------------------------------------------------------------------------
     * PROTECTED ATTRIBUTES
     *--------------------------------------------------------------------------------------------------------------*/         
    protected:
        Daemon_t*									m_daemon;
        int											m_port;
        
    /*--------------------------------------------------------------------------------------------------------------
     * PRIVATE ATTRIBUTES
     *--------------------------------------------------------------------------------------------------------------*/        
    private:    
        static std::string							m_index_page;
        
    /*--------------------------------------------------------------------------------------------------------------
     * PRIVATE METHODS
     *--------------------------------------------------------------------------------------------------------------*/ 
    private:
        static int				http_dispatcher                 (void* cls, struct MHD_Connection* connection, const char* url, 
                                                                 const char* method, const char* version, const char* upload_data, 
                                                                 size_t* upload_data_size, void** ptr);
        
        
        
    /*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/
    public:
                                webServer						(int port = 8888);
        virtual                 ~webServer						();
        virtual bool			Run                             (void);
        virtual int				Get_port                        () const { return m_port; }
		virtual void	        Set_port                        (const int port) { m_port = port; }
	};
}
#endif // WEBSERVER_H

