package appletUtils;

import java.applet.Applet;
import java.awt.FileDialog;
import java.awt.Frame;



public class FileHelper extends Applet
{

            private static final long serialVersionUID = 1L;
            private String m_path;
            public FileHelper() {}
            
            public String GetFullPathToFile()
            {
            	Frame parent = new Frame();
				FileDialog fd = new FileDialog(parent, "APPLET : File Helper", 0);
				fd.setVisible(true);
				return (new StringBuilder(String.valueOf(fd.getDirectory()))).append(fd.getFile()).toString();
            }
            
            @SuppressWarnings("unchecked")
			public String GetFullPathToFileSec()
            {
	            java.security.AccessController.doPrivileged
	            ( 
	            	new java.security.PrivilegedAction() 
	            	{                        
	            		public Object run()
	            		{                            
	            			Frame parent = new Frame();
	        				FileDialog fd = new FileDialog(parent, "APPLET : File Helper", 0);
	        				fd.setVisible(true);
	        				m_path = (new StringBuilder(String.valueOf(fd.getDirectory()))).append(fd.getFile()).toString();
	        				return null;
	                    }                
	            	}
	            );           
	            return m_path;
            }
            
            /*
            public String GetTest_1()
            {
            	return "texto desde applet";
            }
           */
            public void init()
        	{
            	// Decomentar para hacer pruebas. 
            	// new FileHelper().GetFullPathToFileSec();
        	}                   
}


