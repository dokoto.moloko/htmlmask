#summary One-sentence summary of this page.

= Introduction =

This piece of code try to wrap any command line program to execute it from a html browser.


= Details =
Dependencies: 
- LibMicroHttpd

==HOW WORKS ?==
Imagine that you want cutting a film with ffmpeg in small pieces, the common way would be to open terminal and write :

*
ffmpeg -y -i |DIR|FILE_IN.avi -ss 00:05:00 -t 60 -threads 2 -s 320x240 -r 29.97 -threads 1 -pix_fmt yuv420p -g 300 -qmin 3 -b 512k -async 50 -acodec libmp3lame -ar 44100 -ac 2 -ab 128k |DIR|FILE_CUT.avi
*

But if we use htmlMask, we just need to create two files: one to config and another one to map the parameters.

*CONFIG FILE:*
{{{
<!--
HOWTO /Docs/configurations.txt
Mask of FFMPEG WINDOWS
ffmpeg -y -i $1 -ss $3 -t $4 -threads 2 -s 320x240 -r 29.97 -threads 1 -pix_fmt yuv420p -g 300 -qmin 3 -b 512k -async 50 -acodec libmp3lame -ar 44100 -ac 2 -ab 128k $2 ;	
-->
<?xml version='1.0' encoding='UTF-8' standalone='no' ?>
<Config FormatVersion='1'>
	<TerminalProgram listen_port='4444' fullPath='ffmpeg' />
	<HtmlGui root_folder='ConversionVideo' name_of_default_page='index.html' root_html_path='/Users/dokoto/www/'/>
</Config>
}}}

*MAP FILE*
{{{
<!--
HOWTO /Docs/configurations.txt

EJEMPLO : Cortar peliculas con FFMPEG
-->
<?xml version='1.0' encoding='UTF-8' standalone='no' ?>
<Map FormatVersion='1'>
	<Fields>	
		<Field>
			<CommandArgument id='y' format='-%s' parameter_type='param' value='' position='1' />				
			<HtmlArgument id='html_y' type='text' value='' label='' disable='true' hidden='true' />		
		</Field>	
		<Field>
			<CommandArgument id='i' format='-%s %s' parameter_type='both' value='' position='2' />				
			<HtmlArgument id='html_i' type='file' value='' label='Video de Entrada' disable='false' hidden='false' />		
		</Field>	
		<Field>
			<CommandArgument id='ss' format='-%s %s' parameter_type='both' value='' position='3' />				
			<HtmlArgument id='html_ss' type='text' value='' label='Punto de inicio de corte [00:00:00]' disable='false' hidden='false' />		
		</Field>			
		<Field>
			<CommandArgument id='t' format='-%s %s' parameter_type='both' value='' position='4' />				
			<HtmlArgument id='html_t' type='text' value='' label='Numero de segundos a cortar' disable='false' hidden='false' />		
		</Field>		
		<Field>
			<CommandArgument id='threads' format='-%s %s' parameter_type='both' value='2' position='5' />				
			<HtmlArgument id='html_threads' type='text' value='2' label='' disable='true' hidden='true' />		
		</Field>			
		<Field>
			<CommandArgument id='s' format='-%s %s' parameter_type='both' value='' position='7' />				
			<HtmlArgument id='html_s' type='select' default='320x240' value='320x240#Baja Resolucion|640x480#Resolucion Media|800x600#Resolucion Alta' label='Resolucion de Salida' disable='true' hidden='false' />		
		</Field>			
		<Field>
			<CommandArgument id='r' format='-%s %s' parameter_type='both' value='29.97' position='8' />				
			<HtmlArgument id='html_r' type='text' value='29.97' label='' disable='true' hidden='true' />		
		</Field>
		<Field>
			<CommandArgument id='pix_fmt' format='-%s %s' parameter_type='both' value='yuv420p' position='9' />				
			<HtmlArgument id='html_pix_fmt' type='text' value='yuv420p' label='' disable='true' hidden='true' />		
		</Field>		
		<Field>
			<CommandArgument id='g' format='-%s %s' parameter_type='both' value='300' position='10' />				
			<HtmlArgument id='html_gt' type='text' value='300' label='' disable='true' hidden='true' />		
		</Field>			
		<Field>
			<CommandArgument id='qmin' format='-%s %s' parameter_type='both' value='3' position='11' />				
			<HtmlArgument id='html_qmin' type='text' value='3' label='' disable='true' hidden='true' />		
		</Field>	
		<Field>
			<CommandArgument id='b' format='-%s %s' parameter_type='both' value='512k' position='12' />				
			<HtmlArgument id='html_b' type='text' value='512k' label='' disable='true' hidden='true' />		
		</Field>	
		<Field>
			<CommandArgument id='async' format='-%s %s' parameter_type='both' value='50' position='13' />				
			<HtmlArgument id='html_async' type='text' value='50' label='' disable='true' hidden='true' />		
		</Field>	
		<Field>
			<CommandArgument id='acodec' format='-%s %s' parameter_type='both' value='libmp3lame' position='14' />				
			<HtmlArgument id='html_acodec' type='text' value='libmp3lame' label='' disable='true' hidden='true' />		
		</Field>
		<Field>
			<CommandArgument id='ar' format='-%s %s' parameter_type='both' value='44100' position='15' />				
			<HtmlArgument id='html_ar' type='text' value='44100' label='' disable='true' hidden='true' />		
		</Field>	
		<Field>
			<CommandArgument id='ac' format='-%s %s' parameter_type='both' value='2' position='16' />				
			<HtmlArgument id='html_ac' type='text' value='2' label='' disable='true' hidden='true' />		
		</Field>	
		<Field>
			<CommandArgument id='ab' format='-%s %s' parameter_type='both' value='128k' position='17' />				
			<HtmlArgument id='html_ab' type='text' value='128k' label='' disable='true' hidden='true' />		
		</Field>
		<Field>
			<CommandArgument id='out' format='%s' parameter_type='value' value='' position='18' />				
			<HtmlArgument id='html_out' type='file' value='' label='Video de Salida' disable='false' hidden='false' />		
		</Field>		
	</Fields>
</Map>
}}}

And the last step is open the browser in http://localhost:4444

http://htmlmask.googlecode.com/files/htmlmask1.jpg

http://htmlmask.googlecode.com/files/htmlmask2.jpg

http://htmlmask.googlecode.com/files/htmlmask3.jpg